let path = require('path');
let webpack = require('webpack');
let MiniCssExtractPlugin = require("mini-css-extract-plugin");

let conf = {
    entry: {
        app: [
            './src/main.js',
            './src/main.scss',
        ]
    },
    output: {
        path: path.resolve(__dirname, './dist' ),
        filename: '[name].js',
    },
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]'
                        },
                    },
                    {
                        loader: 'extract-loader',
                    },
                    {
                        loader: 'html-loader',
                        options: {
                            interpolate: true,
                            attrs: ['img:src']
                        }
                    },
                ],
            },
            {
                test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    name: 'images/[hash].[ext]',
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules\/(?!(dom7|ssr-window|swiper)\/).*/,
            }

        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery",
            Tab: "exports-loader?Tab!bootstrap/js/dist/tab",
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000,
        watchContentBase: true,
        // warnings: true,
        overlay: true
    }
};

module.exports = (env, options) => {
    let production = options.mode === 'production';
    conf.devtool = production
                    ? false
                    : 'eval-courcemap';
    conf.plugins.push(new webpack.LoaderOptionsPlugin({
        minimize: production,
    }));
    conf.module.rules[2].options.publicPath = production
                    ? '/dist/'
                    : false;
    return conf;
};
