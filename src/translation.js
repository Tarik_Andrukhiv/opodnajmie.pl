var updateContent = function() {
    $('[lng]').each(function( index, value ) {
        var key = $(value).attr('lng');
        jQuery('[lng="'+key+'"]').html(i18next.t(key));
    });
    $('[lng-title]').each(function( index, value ) {
        var key = $(value).attr('lng-title');
        jQuery('[lng-title="'+key+'"]').attr('title', i18next.t(key));
    });
    $('[lng-value]').each(function( index, value ) {
        var key = $(value).attr('lng-value');
        jQuery('[lng-value="'+key+'"]').attr('value', i18next.t(key));
    });
    $('[lng-placeholder]').each(function( index, value ) {
        var key = $(value).attr('lng-placeholder');
        jQuery('[lng-placeholder="'+key+'"]').attr('placeholder', i18next.t(key));
    });
    $('[lng-data-title]').each(function( index, value ) {
        var key = $(value).attr('lng-data-title');
        jQuery('[lng-data-title="'+key+'"]').attr('data-title', i18next.t(key));
    });
    $('[lng-href]').each(function( index, value ) {
        var key = $(value).attr('lng-href');
        jQuery('[lng-href= "'+key+'"]').attr('href', i18next.t(key));
    });
},
changeLng = function(lng) {
    i18next.changeLanguage(lng);
    jQuery('[data_language]').addClass('active');
    jQuery('[data_language="'+lng+'"]').removeClass('active');
};
if (typeof(i18next) != "undefined") {
    i18next.init({
        lng: 'en',
        resources: {
            pl: {
                translation: {
                    "contact": "kontakt",
                    "your_email": "Twój email",
                    "subscribe": "Zapisz się",
                    "name": "Imię i nazwisko *",
                    "email_address": "Adres e-mail *",
                    "write_your_message_here": "Napisz swoją wiadomość tutaj...",
                    "send": "Wysłać",
                    "all_rights_reserved": "Wszelkie prawa zastrzeżone",
                    "privacy_policy": "Polityka Prywatności",
                    "thank_you_message_sent": "Dziękujemy, wiadomość wysłana.",
                    "thank_you_for_reporting": "Dziękujemy za zgłoszenie.",
                    "server_error_try_again": "Błąd serwera, spróbuj jeszcze raz.",
                    "please_fill_in_the_form_fields_correctly": "Proszę poprawnie uzupełnić pola formularza.",

                }
            },
            en: {
                translation: {
                    "contact": "contact",
                    "your_email": "Your email",
                    "subscribe": "Subscribe",
                    "name": "User name *",
                    "email_address": "Email address *",
                    "write_your_message_here": "Write your message here...",
                    "send": "Send",
                    "all_rights_reserved": "All Rights Reserved",
                    "privacy_policy": "Privacy Policy",
                    "thank_you_message_sent": "Thank you, message sent.",
                    "thank_you_for_reporting": "Thank you for reporting.",
                    "server_error_try_again": "Server error, try again.",
                    "please_fill_in_the_form_fields_correctly": "Please fill in the form fields correctly.",
                }
            }
        }
    }, function(err, t) {
        updateContent();
    });
    i18next.on('languageChanged', function() {
        updateContent();
    });

    $( window ).on('load', function() {
        changeLng('pl');
        // if (navigator.language == 'pl-PL') {
        // } else {
        //     changeLng('en');
        // }
    });
    $(document).on('click', '[data_language]', function() {
        var language = jQuery(this).attr('data_language');
        changeLng(language);
    });
}
