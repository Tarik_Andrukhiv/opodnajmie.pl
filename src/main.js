import $ from 'jquery';
import popper from 'popper.js';
import bootstrap from 'bootstrap';
import AOS from 'aos';
import i18next from 'i18next';
require("imports-loader?$=jquery,i18next!./translation.js");
require("./index.html");

AOS.init({
    disable: 'phone'
});

function year() {
  var d = new Date();
  var n = d.getFullYear();
  document.getElementById("year").innerHTML = n;
}

function tabShow() {
    let url = location.href.replace(/\/$/, ""),
        tabsTable = [];
    if (location.hash.length > 0) {
        var hash = url.split("#");
        if (hash[1].indexOf("?")>=0) {
            (hash[1].split("?")[1] == 'sent')?alert(i18next.t('thank_you_message_sent')):false;
            hash[1] = hash[1].split("?")[0];
            location.hash = hash[1];
        }
        $('.tab-pane').each(function(i,v) {
            tabsTable[i] = $(v).attr('id');
        });
        if (tabsTable.indexOf(hash[1]) >= 0) {
            // $('main').removeClass().addClass(hash[1]);
            $('#'+hash[1]).addClass('show active');
            $('.nav-link').removeClass('active show');
            $('.nav-link[href="#'+hash[1]+'"]').addClass('active');
            return false;
        }else {
            $('#home').addClass('show active');
        }
        $('html,body').animate({
            scrollTop: 0
        });
    }else {
        $('#home').addClass('show active');
    }
}

function aniScr() {
    if ($(window)[0].pageYOffset>1) {
        $('header .navbar, main').addClass('OffsetY');
    }else {
        $('header .navbar, main').removeClass('OffsetY');
    }
}
function partners() {
    $('.partners').append($('#forPartners').html());
}

$(document).ready(function() {
    year();
    tabShow();
    aniScr();
    partners();
    $('[data-toggle="tooltip"]').tooltip()
    $(window).scroll(function() {
        aniScr();
        $('#navbarToggler').collapse('hide');
    });
    $('[toMove]').click(function() {
        var toMove = $(this).attr('toMove'),
            issection = $(toMove).closest('.section').hasClass('section-2'),
            numTop = parseInt($(toMove)[0].offsetTop - ($(window).width() > 992 ? $('.navbar').height():40));
        numTop += issection ? 288 : 0;
        $('[toMove]').removeClass('active'); $(this).addClass('active');
        $( 'html, body' ).animate({ scrollTop: numTop }, 800);
    });
    $('[data-toggle="pill"]').click(function ( event ) {
        $('html,body').animate({
            scrollTop: 0
        });
        event.preventDefault();
        location.hash = $(this).attr('href');
        $('body').removeClass().addClass($(this).attr('aria-controls'));
        $(this).tab('show');
        $('.nav-link').removeClass('active show');
        $('.nav-link[href="'+location.hash+'"]').addClass('active');
        AOS.refresh();
        AOS.refreshHard();
    });
    $('.dropdown-item, .navbar a').click(function () {
        $('#navbarToggler').collapse('hide');
    });
    $('a[data-toggle="pill"]').on('shown.bs.tab', function () {
        AOS.refresh();
        AOS.refreshHard();
    });
    $('img.svg').each(function () {
        var $img = $(this);
        var imgId = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgUrl = $img.attr('src');
        $.get(imgUrl, function (data) {
            var $svg = $(data).find('svg');
            if (typeof imgId !== 'undefined') {
                $svg = $svg.attr('id', imgId);
            }
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            $img.replaceWith($svg);
        });
    });
    $( "#contact-form" ).submit(function( event ){
    	event.preventDefault();
    	$('input').removeClass('has-error');
    	$('textarea').removeClass('has-error');
    	var _name = $('[name="c-name"]').val();
    	var _email = $('[name="c-email"]').val();
    	var _messages = $('[name="c-message"]').val();

    	var error = false;

    	if (_name.length < 4) {
    		$('[name="c-name"]').closest( ".form-group" ).addClass('has-error');
    		error = true;
    	} else {
    		$('[name="c-name"]').closest( ".form-group" ).removeClass('has-error');
    	}

    	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    	if (!_email.match(filter)) {
    		$('[name="c-email"]').closest( ".form-group" ).addClass('has-error');
    		error = true;
    	} else {
    		$('[name="c-email"]').closest( ".form-group" ).removeClass('has-error');
    	}

    	if (_messages=='') {
    		$('[name="c-message"]').closest( ".form-group" ).addClass('has-error');
    		error = true;
    	} else {
    		$('[name="c-message"]').closest( ".form-group" ).removeClass('has-error');
    	}

    	if (!error) {
    		jQuery.ajax({
    			method: 'POST',
    			url: "../messages/contact.php",
    			data: {'Name':_name, 'Email':_email, 'Messages': _messages},
    			beforeSend: function(){jQuery('button[type="submit"]').attr('disabled', 'disabled')},
    			complete: function(){jQuery('button[type="submit"]').removeAttr('disabled');},
    			success: function(data){
    				if (data != 'OK') {
    					alert(i18next.t('thank_you_for_reporting'), 4000);
    					$('[name="c-name"]').val("");
    					$('[name="c-email"]').val("");
    					$('[name="c-message"]').val("");
    				} else {
                        alert(i18next.t('server_error_try_again'), 4000);
    				}
    			}
    		});

    		return false;
    	} else {
            alert(i18next.t('please_fill_in_the_form_fields_correctly'), 4000);
    	}
    });
});

$('.main').css('opacity', 0);
window.addEventListener('load', function () {
    $('.main').css('opacity', 1).addClass('load');
    AOS.refresh();
    AOS.refreshHard();
    setTimeout(function(){
        $('.preloader').animate({
            'opacity': 0
        }, 800, function() {
            $(this).css('display', 'none');
        });
    }, 100);
}, false);
